import java.math.BigInteger
import java.security.MessageDigest

fun main(args: Array<String>) {
    val arr = Array<Int>(10, { i-> i + 1 })
    val sTime = System.nanoTime()
    arr.sort()
    val eTime = System.nanoTime()
    println(arr.contentToString())
    println(eTime - sTime)

    val str = "Hello World"
    val md = MessageDigest.getInstance("SHA-512")
    md.reset()
    md.update(str.toByteArray(Charsets.UTF_8))
    println(String.format("%040x", BigInteger(1, md.digest())))

    println(Something.I.count())
}

enum class Something {
    HELLO, WORLD, I, AM, HERE;

    fun count() {
        println(this.ordinal)
    }
}